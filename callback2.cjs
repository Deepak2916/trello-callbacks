/* Problem 2: Write a function that will return all lists that belong to a board 
based on the boardID that is passed to it from the given data in lists.json. 
Then pass control back to the code that called it by using a callback function.
*/
const fs = require('fs')
const path = require('path')

// const boardsPath = path.resolve('boards.json')
const listsPath = path.resolve('lists.json')

function listsThatBelongToBoard(boardID, callback) {

    setTimeout(() => {

        fs.readFile(listsPath, 'utf-8', (error, data) => {
            if (error) {
                callback(error)

            } else {
                data = JSON.parse(data)
                if (data[boardID] === undefined) {

                    callback(null, [])
                } else {
                    callback(null, data[boardID])
                }
            }
        })

    }, 1000 * 2)

}


module.exports = listsThatBelongToBoard