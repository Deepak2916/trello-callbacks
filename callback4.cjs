/* 
    Problem 4: Write a function that will use the previously written functions to 
    get the following information. You do not need to pass control back to the code 
    that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
function problem4(thanosId) {

    function getInformationFromThanosBoards(thanosId, callback1, callback2) {
        const callbackProblem1 = require('./callback1.cjs')
        callbackProblem1(thanosId, (error, data) => {
            if (error) {
                callback1(error)
            }
            else {
                // console.log(data)
                callback1(null, data, callback2)
            }
        })


    }
    getInformationFromThanosBoards(thanosId, listsForTheThanosBoard, cardsForTheMindList)

    function listsForTheThanosBoard(error, thanosData, callback) {
        if (error) {
            console.log(error)
        } else {

            const callbackProblem2 = require('./callback2.cjs')
            // console.log(thanosData);
            callbackProblem2(thanosData[0].id, (error, data) => {
                if (error) {
                    callback(error)
                } else {
                    data = [...thanosData, ...data]
                    callback(null, data)
                }
            })
        }

    }

    function cardsForTheMindList(error, data) {
        if (error) {
            console.log(error)
        }
        else {

            const callbackProblem3 = require('./callback3.cjs')

            const mindData = data.filter(data => {
                return data.name === 'Mind'
            })
            callbackProblem3(mindData[0].id, (error, mindData) => {
                if (error) {
                    console.log(error)
                }
                else {
                    data = [data, mindData]
                    console.log(data)

                }
            })
        }


    }
}

function setTimeToExecute(thanosId){
    setTimeout(()=>{
        problem4(thanosId)
    },2*100)
}
module.exports = setTimeToExecute