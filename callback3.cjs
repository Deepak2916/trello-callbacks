/* 
    Problem 3: Write a function that will return all cards that belong to a 
    particular list based on the listID that is passed to it from the given data in cards.
    json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require('fs')
const path = require('path')

// const boardsPath = path.resolve('boards.json')
const cardsPath = path.resolve('cards.json')

function listsThatBelongToBoard(listID, callback) {

    setTimeout(() => {

        fs.readFile(cardsPath, 'utf-8', (error, data) => {
            if (error) {
                callback(error)

            } else {
                data = JSON.parse(data)
                if (data[listID] === undefined) {
                    callback(null, [])
                }
                else {
                    callback(null, data[listID])
                }


            }
        })

    }, 1000 * 2)

}


module.exports = listsThatBelongToBoard