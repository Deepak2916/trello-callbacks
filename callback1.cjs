

/*Problem 1: Write a function that will return a particular board's information 
based on the boardID that is passed from the given list of boards in boards.
json and then pass control back to the code that called it by using a callback function.*/
const fs = require('fs')
const path = require('path')

const boardsPath = path.resolve('boards.json')

function boardInformationBasedOnTheBoardID(boardID, callback) {
    
    setTimeout(() => {

        fs.readFile(boardsPath, 'utf-8', (error, data) => {
            if (error) {
                callback(error)

            } else {
                data = JSON.parse(data)
                const informationOfBoardId = data.filter(board => {
                    return board["id"] === boardID
                })
                callback(null, informationOfBoardId)
            }
        })
        
    }, 1000 * 2)

}





module.exports = boardInformationBasedOnTheBoardID