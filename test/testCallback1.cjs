const callbackProblem1 = require('../callback1.cjs')

function callback(error, data) {
    if (error) {
        console.log(error)
    }
    else {
        console.log(data)
    }
}
callbackProblem1("mcu453ed", callback)