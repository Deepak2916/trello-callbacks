const callbackProblem2 = require('../callback2.cjs')

function callback(error, data) {
    if (error) {
        console.log(error)
    }
    else {
        console.log(data)
    }
}
callbackProblem2("mcu453ed", callback)